import sys
import os

import numpy as np
from OpenGL.GL import *
import transformations as tr
import basic_shapes as bs
import easy_shaders as es
import scene_graph as sg
import glfw


class MazeView:
    def __init__(self, model):
        self.model = model
        self.pipeline = es.SimpleTransformShaderProgram()
        glUseProgram(self.pipeline.shaderProgram)
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
        squares_h, squares_v = self.model.matrix.shape
        x_scale = 2/squares_h
        y_scale = 2/squares_v
        self.root = sg.SceneGraphNode("root")
        self.wall = createWallTile(x_scale, y_scale)
        self.floor = createFloorTile(x_scale, y_scale)
        self.pedro = createPedro(x_scale, y_scale)
        self.coin = createCoin(min(x_scale, y_scale)*0.8)
        self.fillMaze()
        # makes it look clearer
        self.root.childs.append(createGrid(x_scale, y_scale))

    def render(self, window):
        glClear(GL_COLOR_BUFFER_BIT)
        sg.drawSceneGraphNode(self.root, self.pipeline, "transform")

    def fillMaze(self):
        m = self.model.matrix
        squares_h, squares_v = m.shape
        x_scale = 2/squares_h
        y_scale = 2/squares_v
        for i in range(squares_h):
            for j in range(squares_v):
                n = self.add(self.floor, i, j, x_scale, y_scale)
                self.changeTile(i, j, toChange=n)

    def add(self, node, i, j, x_scale, y_scale):
        translated = sg.SceneGraphNode("translated "+str(i)+" "+str(j))
        translated.childs.append(node)
        translated.transform = tr.matmul([
            tr.translate(-1+(0.5+i)*x_scale, -1+(0.5+j)*y_scale, 0),
        ])
        self.root.childs.append(translated)
        return translated

    def changeTile(self, i, j, toChange=None):
        if not toChange:
            name = "translated "+str(i)+" "+str(j)
            toChange = sg.findNode(self.root, name)
        value = self.model.matrix[i, j]
        if value == 1:
            toChange.childs = [self.wall]
        else:
            toChange.childs = [self.floor]
        if value == 2:
            toChange.childs.append(self.pedro)
        elif value == 3:
            toChange.childs.append(self.coin)


def createGrid(width, height):
    black_square = es.toGPUShape(bs.createColorQuad(0, 0, 0))
    whole_grid = sg.SceneGraphNode("whole_grid")

    vertical_bar = sg.SceneGraphNode("generic_v_bar")
    vertical_bar.transform = tr.scale(0.01, 2, 1)
    vertical_bar.childs = [black_square]
    vertical_reps = int(1/width)
    for i in range(-vertical_reps, vertical_reps+1):
        vertical_bar_i = sg.SceneGraphNode("v_bar_"+str(i))
        vertical_bar_i.childs = [vertical_bar]
        vertical_bar_i.transform = tr.translate(i*width, 0, 0)
        whole_grid.childs.append(vertical_bar_i)

    horizontal_bar = sg.SceneGraphNode("generic_h_bar")
    horizontal_bar.transform = tr.scale(2, 0.01, 1)
    horizontal_bar.childs = [black_square]
    horizontal_reps = int(1/height)
    for i in range(-horizontal_reps, horizontal_reps+1):
        horizontal_bar_i = sg.SceneGraphNode("h_bar_"+str(i))
        horizontal_bar_i.childs = [horizontal_bar]
        horizontal_bar_i.transform = tr.translate(0, i*height, 0)
        whole_grid.childs.append(horizontal_bar_i)

    return whole_grid


def createCoin(diameter):
    """Creates a coin centered in (0, 0)"""
    yellow_circle = es.toGPUShape(bs.createColorCircle(1, 1, 0, 10))

    coin = sg.SceneGraphNode("generic_coin")
    coin.transform = tr.uniformScale(diameter/2)
    coin.childs = [yellow_circle]

    return coin


def createWallTile(width, height):
    """Creates a square wall tile centered in (0, 0)"""
    brick_square = es.toGPUShape(bs.createColorQuad(0.796, 0.403, 0.05))
    clay_square = es.toGPUShape(bs.createColorQuad(0.313, 0.36, 0.37))

    # basic brick surface
    brick = sg.SceneGraphNode("generic_brick")
    brick.transform = tr.scale(width, height, 1)
    brick.childs.append(brick_square)

    # horizontal clay separation lines
    h_clay = sg.SceneGraphNode("h_clay")
    h_clay.transform = tr.scale(1, 0.02, 1)
    h_clay.childs.append(clay_square)

    # vertical clay separation lines
    v_clay = sg.SceneGraphNode("v_clay")
    v_clay.transform = tr.scale(0.02, 0.24, 1)
    v_clay.childs.append(clay_square)

    for i in range(-1, 2):
        h_clay_i = sg.SceneGraphNode("h_clay_"+str(i))
        h_clay_i.transform = tr.translate(0, i*0.25, 0)
        h_clay_i.childs.append(h_clay)
        brick.childs.append(h_clay_i)

    for i in range(2):
        v_clay_i = sg.SceneGraphNode("v_clay_a"+str(i))
        v_clay_i.transform = tr.translate(0, -0.5*i+0.125, 0)
        v_clay_i.childs.append(v_clay)
        brick.childs.append(v_clay_i)

    for i in range(2):
        for j in range(2):
            v_clay_i = sg.SceneGraphNode("v_clay_b"+str(i)+str(j))
            v_clay_i.transform = tr.translate(0.5*i-0.25, 0.5*j-0.125, 0)
            v_clay_i.childs.append(v_clay)
            brick.childs.append(v_clay_i)

    return brick


def createPedro(x_scale, y_scale):
    """Creates a stick figure, a little more than one block tall,
    positioned in such a way that it can just be put on a square"""

    circle = es.toGPUShape(bs.createColorCircle(0, 1, 0, 8))
    blue_square = es.toGPUShape(bs.createColorQuad(0, 1, 0))

    blue_stick = sg.SceneGraphNode("blue_stick")
    blue_stick.childs = [blue_square]
    blue_stick.transform = tr.scale(0.04, 1, 1)

    pedro = sg.SceneGraphNode("pedro")
    pedro.transform = tr.scale(x_scale, y_scale, 1)

    torso = sg.SceneGraphNode("torso")
    torso.childs.append(blue_stick)
    torso.transform = tr.matmul([
        tr.scale(1, 0.4, 1),
    ])

    left_arm = sg.SceneGraphNode("left_arm")
    left_arm.childs.append(blue_stick)
    left_arm.transform = tr.matmul([
        tr.translate(-0.1, 0, 0),
        tr.rotationZ(-np.pi/4),
        tr.scale(1, np.hypot(0.2, 0.2), 1),
    ])

    left_leg = sg.SceneGraphNode("left_leg")
    left_leg.childs.append(blue_stick)
    left_leg.transform = tr.matmul([
        tr.translate(-0.1, -0.3, 0),
        tr.rotationZ(-np.pi/4),
        tr.scale(1, np.hypot(0.2, 0.2), 1),
    ])

    right_side = sg.SceneGraphNode("right_side")
    right_side.childs = [left_arm, left_leg]
    right_side.transform = tr.reflectionZ(np.pi/2)

    head = sg.SceneGraphNode("head")
    head.childs = [circle]
    head.transform = tr.matmul([
        tr.translate(0, 0.3, 0),
        tr.uniformScale(0.1),
    ])
    pedro.childs = [torso, left_leg, left_arm, right_side, head]
    return pedro


def createFloorTile(width, height):
    """Creates a floor tile centered in (0, 0)
    with the given by width and height"""
    black_square = es.toGPUShape(bs.createColorQuad(0.59, 0.59, 0.59))
    white_square = es.toGPUShape(bs.createColorQuad(1, 1, 1))

    floor = sg.SceneGraphNode("floor_generic")
    floor.transform = tr.scale(width, height, 1)
    floor.childs.append(white_square)

    upper_black = sg.SceneGraphNode("upper_black")
    upper_black.transform = tr.matmul([
        tr.translate(0.25, 0.25, 0),
        tr.scale(0.5, 0.5, 1),
    ])
    upper_black.childs.append(black_square)
    floor.childs.append(upper_black)

    lower_black = sg.SceneGraphNode("lower_black")
    lower_black.transform = tr.matmul([
        tr.translate(-0.25, -0.25, 0),
        tr.scale(0.5, 0.5, 1),
    ])
    lower_black.childs.append(black_square)
    floor.childs.append(lower_black)

    return floor


class Maze:
    def __init__(self, source):
        if type(source) == tuple:
            # then it's just the desired dimensions
            assert len(source) == 2
            self.matrix = np.zeros(source)
        elif type(source) == str:
            self.matrix = np.load(source)
        else:
            raise TypeError

    def check_playable(self):
        one_pedro = len(np.transpose((self.matrix == 2).nonzero())) == 1
        some_coins = len(np.transpose((self.matrix == 3).nonzero())) > 0
        return one_pedro and some_coins

    def check_finished(self):
        return len(np.transpose((self.matrix == 3).nonzero())) == 0

    def save(self, dest):
        np.save(dest, self.matrix)

    def flipTo(num):
        def f(self, x, y):
            if self.matrix[x, y] == 0:
                self.matrix[x, y] = num
            elif self.matrix[x, y] == num:
                self.matrix[x, y] = 0
        return f

    flipCoin = flipTo(3)
    flipWall = flipTo(1)

    def placePedro(self, x, y):
        """Change Pedro's location. Will return a list
        with the coordinates of the places where he was"""
        previous = np.transpose((self.matrix == 2).nonzero())
        # if he was in two places, that's a problem
        assert len(previous) <= 1
        self.matrix[self.matrix == 2] = 0
        self.matrix[x, y] = 2

        return previous

    def move_pedro_up(self):
        previous = np.transpose((self.matrix == 2).nonzero())
        x, y = previous[0]
        try:
            desired_location = self.matrix[x, y+1]
        except IndexError:
            return []
        if desired_location == 0 or desired_location == 3:
            self.placePedro(x, y+1)

        return [(x, y), (x, y+1)]

    def move_pedro_down(self):
        previous = np.transpose((self.matrix == 2).nonzero())
        x, y = previous[0]
        try:
            desired_location = self.matrix[x, y-1]
        except IndexError:
            return []
        if desired_location == 0 or desired_location == 3:
            self.placePedro(x, y-1)

        return [(x, y), (x, y-1)]

    def move_pedro_left(self):
        previous = np.transpose((self.matrix == 2).nonzero())
        x, y = previous[0]
        try:
            desired_location = self.matrix[x-1, y]
        except IndexError:
            return []
        if desired_location == 0 or desired_location == 3:
            self.placePedro(x-1, y)

        return [(x, y), (x-1, y)]

    def move_pedro_right(self):
        previous = np.transpose((self.matrix == 2).nonzero())
        x, y = previous[0]
        try:
            desired_location = self.matrix[x+1, y]
        except IndexError:
            return []
        if desired_location == 0 or desired_location == 3:
            self.placePedro(x+1, y)

        return [(x, y), (x+1, y)]
