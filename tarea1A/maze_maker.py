import sys
import os

import numpy as np
from OpenGL.GL import *
import transformations as tr
import basic_shapes as bs
import easy_shaders as es
import scene_graph as sg
import glfw

from maze_common import Maze, MazeView


class Controller:
    def __init__(self, model):
        self.model = model

    def mainLoop(self):
        if not glfw.init():
            return

        squares_h, squares_v = self.model.matrix.shape
        titulo = "Creador de Laberintos"
        # Map squares will be 30x30 pixels
        window = glfw.create_window(
            squares_h*30, squares_v*30, titulo, None, None)
        if not window:
            glfw.terminate()
            return
        glfw.make_context_current(window)

        glfw.set_framebuffer_size_callback(window, self.updateViewport)
        glfw.set_mouse_button_callback(window, self.mouse_input)
        glfw.set_key_callback(window, self.keyboard_input)

        self.view = MazeView(self.model)

        while not glfw.window_should_close(window):
            self.view.render(window)

            glfw.swap_buffers(window)
            glfw.wait_events()

        glfw.terminate()

    def get_square_under_cursor(self, window):
        squares_h, squares_v = self.model.matrix.shape
        (x, y) = glfw.get_cursor_pos(window)

        # convert coordinates to squares
        (w, h) = glfw.get_framebuffer_size(window)
        x = x * squares_h // (w + 1)
        y = y * squares_v // (h + 1)
        y = squares_v - y - 1  # y is inverted
        return (int(x), int(y))

    def mouse_input(self, window, button, action, mods):
        squares_h, squares_v = self.model.matrix.shape
        if action != glfw.PRESS:
            return

        (x, y) = self.get_square_under_cursor(window)
        if button == glfw.MOUSE_BUTTON_1:
            self.model.flipWall(x, y)
        elif button == glfw.MOUSE_BUTTON_2:
            self.model.flipCoin(x, y)
        elif button == glfw.MOUSE_BUTTON_3:
            previous = self.model.placePedro(x, y)
            for (x2, y2) in previous:
                self.view.changeTile(x2, y2)
        self.view.changeTile(x, y)

    def keyboard_input(self, window, key, scancode, action, mods):
        squares_h, squares_v = self.model.matrix.shape
        if action != glfw.PRESS:
            return

        (x, y) = self.get_square_under_cursor(window)
        if key == glfw.KEY_1:
            previous = self.model.placePedro(x, y)
            self.view.changeTile(x, y)
            for (x2, y2) in previous:
                self.view.changeTile(x2, y2)

    def updateViewport(self, _, w, h):
        glViewport(0, 0, w, h)


def main():
    try:
        mazeFileName = sys.argv[1]
    except IndexError:
        print("You need to provide a file name for the maze!")
        return

    try:
        [strv, strh] = sys.argv[2].split('x')
        squares_v, squares_h = int(strv), int(strh)
        if squares_v > 20 or squares_h > 40:
            raise ValueError
    except IndexError:
        squares_v, squares_h = 10, 20
    except ValueError:
        print(sys.argv[2], "is not a valid size", file=sys.stderr)
        return

    if os.path.exists(mazeFileName):
        maze = Maze(mazeFileName)
    else:
        maze = Maze((squares_h, squares_v))
    controller = Controller(maze)
    controller.mainLoop()
    maze.save(mazeFileName)


if __name__ == "__main__":
    main()
