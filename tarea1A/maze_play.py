import sys
import os
import time

import numpy as np
from OpenGL.GL import *
import transformations as tr
import basic_shapes as bs
import easy_shaders as es
import scene_graph as sg
import glfw

from maze_common import Maze, MazeView


class Controller:
    def __init__(self, model):
        self.model = model

    def mainLoop(self):
        if not glfw.init():
            return

        squares_h, squares_v = self.model.matrix.shape
        titulo = "Aventuras de Don Pedro"
        # Map squares will be 30x30 pixels
        window = glfw.create_window(
            squares_h*30, squares_v*30, titulo, None, None)
        if not window:
            glfw.terminate()
            return
        glfw.make_context_current(window)

        glfw.set_key_callback(window, self.keyboard_input)
        glfw.set_framebuffer_size_callback(window, self.updateViewport)

        self.view = MazeView(self.model)

        initial_time = time.time()
        self.moves = 0

        while not glfw.window_should_close(window):
            self.view.render(window)

            glfw.swap_buffers(window)
            if self.model.check_finished():
                print("Terminó el juego!")
                print("Se realizaron", self.moves, "movidas")
                print("Transcurrieron", time.time()-initial_time, "segundos")
                break
            glfw.wait_events()

        glfw.terminate()

    def keyboard_input(self, window, key, scancode, action, mods):
        if action != glfw.PRESS:
            return

        if key == glfw.KEY_UP:
            to_update = self.model.move_pedro_up()
        elif key == glfw.KEY_DOWN:
            to_update = self.model.move_pedro_down()
        elif key == glfw.KEY_LEFT:
            to_update = self.model.move_pedro_left()
        elif key == glfw.KEY_RIGHT:
            to_update = self.model.move_pedro_right()
        else:
            return
        for (x, y) in to_update:
            self.view.changeTile(x, y)
        if len(to_update) > 0:
            self.moves += 1

    def updateViewport(self, _, w, h):
        glViewport(0, 0, w, h)


def main():
    try:
        mazeFileName = sys.argv[1]
    except IndexError:
        print("You need to provide a file name for the maze!")
        return

    try:
        maze = Maze(sys.argv[1])
    except:
        print("Error reading maze file "+sys.argv[1])
        return
    if not maze.check_playable():
        print("The maze file given is not playable")
        return
    controller = Controller(maze)
    controller.mainLoop()


if __name__ == "__main__":
    main()
