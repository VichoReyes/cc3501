import glob
import numpy as np

def normalize(filename):
    matrix = np.load(filename)
    if 0.999999999 < np.sum(matrix) < 1.0000001:
        return
    matrix /= np.sum(matrix)
    np.save(filename, matrix)

if __name__ == "__main__":
    files = glob.glob('*.npy')
    for filename in files:
        normalize(filename)
