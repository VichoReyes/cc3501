---
title: "Explorador Interactivo de Malla Curricular"
author: [Vicente Reyes]
date: "2019-10-14"
lang: "es"
...

# Explorador Interactivo de Malla Curricular

Lo siguiente se trata de un programa que permite explorar una malla curricular universitaria dada por un archivo JSON. La malla del Departamento de Ciencias de la Computación de la Universidad de Chile se entrega como ejemplo.

Para escribir este programa, se usó Python. La primera vez que se corre, el archivo  `texture_gen.py` genera texturas que son luego cargadas en `visor_malla.py`.
Una textura de particular interés es `malla.g.png`, que usa transparencia para que los 
objetos detrás de la textura sean visibles, lo cual funciona gracias a que el _fragment shader_ del programa descarta la renderización de ciertas texturas si son muy transparentes.
Además, esta textura contiene información de todos los cursos de la malla, 
y el programa es lo suficientemente inteligente como para extraer las partes apropiadas
para cada objeto del grafo de escena, y para solo cargar la imagen una vez.

Con respecto al diseño del programa, este se basa en el patrón Model-View-Controller,
aunque probablemente no sea el mejor ejemplo de éste. El código no es muy bonito, pero funciona.
Algo que quedó particularmente feo es el seguimiento automático de la cámara,
que se basa en múltiples atributos del controlador que especifican:

- Cuánto se ha recorrido hasta ahora
- Si el seguimiento automático está activado
- La matriz de Hermite que determina el movimiento

Muchos de los cuales se resetean a `None` en las partes apropiadas del código. El siguiente diagrama debería servir para entender lo complejas que son las relaciones entre los métodos:

![Diagrama Hermite](diagrama_hermite.png){ width=400px }

## Cómo usar?

El programa se inicia con el comando

```
$ python visor_malla.py
```

Después de una pequeña pausa, se debería abrir una ventana con la malla. En ella, se pueden usar las siguientes teclas:

- WASD: Controlan el curso seleccionado (verde).
- Flechas del teclado: Mueven la cámara.
- 1, 2: Controlan el seguimiento automático de la cámara.
- Esc: Sale del programa.

## Screenshots

![Screenshot 0](screenshot0.png){ width=400px }

![Screenshot 1](screenshot1.png){ width=400px }

También hay un video de demostración disponible en el repositorio.
