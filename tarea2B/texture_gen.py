import json
from PIL import Image, ImageDraw, ImageFont
import textwrap
import os

colors = {
    "black": (0, 0, 0, 255),
    "blue": (0, 0, 128, 255),
    "green": (0, 128, 0, 255),
    "purple": (75, 0, 130, 255)
}


def generate_colors():
    for color_name, color in colors.items():
        img = Image.new('RGBA', (1, 1), color=color)
        img.save(color_name+".g.png")


def generate(json_filename):
    if os.path.exists("malla.g.png"):
        return
    print("Texture files not found. Generating...")
    with open(json_filename) as json_file:
        malla = json.load(json_file)

    malla_pics = []
    for semestre in malla:
        sem_pics = []
        for curso in semestre["cursos"]:
            sem_pics.append(gen_curso(curso["codigo"], curso["nombre"]))
        malla_pics.append(sem_pics)
    generate_png(malla_pics)
    generate_colors()


transparent = (0, 0, 0, 0)
texture_side = 150


def generate_png(malla_pics):
    height = len(malla_pics) * texture_side
    width = max(map(len, malla_pics)) * texture_side
    img = Image.new('RGBA', (width, height), transparent)
    for i, semestre in enumerate(malla_pics):
        for j, curso in enumerate(semestre):
            img.paste(curso, box=(texture_side*j, texture_side*i))
    img.save("malla.g.png")


def gen_curso(codigo, nombre):
    nombre = textwrap.fill(nombre, 21) + "\n\n"+codigo
    img = Image.new('RGBA', (texture_side, texture_side), transparent)
    d = ImageDraw.Draw(img)
    d.text((10, 10), nombre,  fill=(255, 255, 255))
    return img


if __name__ == "__main__":
    generate("malla.json")
