import glfw
from OpenGL.GL import *
import OpenGL.GL.shaders
import numpy as np
import sys
import json

import scene_graph as sg
import transformations as tr
import basic_shapes as bs
import easy_shaders as es

import texture_gen


BAJAR = 0
REQUISITO = 1
SELECCIONAR = 2


camera_distance = 6


class View:
    def __init__(self, malla):
        self.generate_cubes()
        self.generate_graph(malla)

    def generate_cubes(self):
        black = bs.createTextureCube("black.g.png")
        self.black = es.toGPUShape(black, GL_CLAMP, GL_NEAREST)
        blue = bs.createTextureCube("blue.g.png")
        self.blue = es.toGPUShape(blue, GL_CLAMP, GL_NEAREST)
        green = bs.createTextureCube("green.g.png")
        self.green = es.toGPUShape(green, GL_CLAMP, GL_NEAREST)
        purple = bs.createTextureCube("purple.g.png")
        self.purple = es.toGPUShape(purple, GL_CLAMP, GL_NEAREST)

    def course_node(self, curso, bx, ex, by, ey):
        codigo = curso["codigo"]
        text_shape = bs.createTextureQuadCuts(
            "malla.g.png", bx, ex, by, ey)
        gpu_text = es.toGPUShape(text_shape, GL_REPEAT, GL_LINEAR)
        text_node = sg.SceneGraphNode(codigo+" text")
        text_node.transform = tr.translate(0, 0, 0.51)
        text_node.childs = [gpu_text]
        cube_node = sg.SceneGraphNode(codigo+" cube")
        cube_node.childs = [self.black]
        node = sg.SceneGraphNode(codigo)
        node.childs = [cube_node, text_node]
        return node

    def generate_graph(self, malla):
        raiz = sg.SceneGraphNode("raiz")
        generate_wall(raiz)
        semestres = len(malla)
        cursos = max(map(lambda x: len(x["cursos"]), malla))
        for j, semestre in enumerate(malla):
            sem = sg.SceneGraphNode(semestre["semestre"])
            sem.transform = tr.translate(0, -j, 0)
            for i, curso in enumerate(semestre["cursos"]):
                bx, ex = i/cursos, (i+1)/cursos
                by, ey = j/semestres, (j+1)/semestres
                node = self.course_node(curso, bx, ex, by, ey)
                node.transform = tr.translate(i, 0, 0)
                sem.childs.append(node)
            raiz.childs.append(sem)
        self.raiz = raiz

    def update(self, codigo, accion):
        nodo_curso = sg.findNode(self.raiz, codigo)
        if accion == BAJAR:
            nodo_curso.transform = np.matmul(
                tr.translate(0, 0, -1), nodo_curso.transform)
            nodo_curso.childs[0].childs[0] = self.black
        elif accion == REQUISITO:
            nodo_curso.transform = np.matmul(
                tr.translate(0, 0, 1), nodo_curso.transform)
            nodo_curso.childs[0].childs[0] = self.blue
        elif accion == SELECCIONAR:
            nodo_curso.transform = np.matmul(
                tr.translate(0, 0, 1), nodo_curso.transform)
            nodo_curso.childs[0].childs[0] = self.green

    def show(self, pipeline):
        sg.drawSceneGraphNode(self.raiz, pipeline, "model")

def generate_wall(raiz):
    wall = sg.SceneGraphNode("wall")
    wall.transform = np.matmul(tr.translate(4, -6, -4), tr.uniformScale(100))
    bs_wall = bs.createTextureQuad("wall.png", 15, 15)
    gpu_wall = es.toGPUShape(bs_wall, GL_REPEAT, GL_LINEAR)
    wall.childs = [gpu_wall]
    raiz.childs.append(wall)

class Model:
    def __init__(self, malla, view: View):
        self.malla = malla
        self.sem_i = 0
        self.cur_i = 0
        self.view = view
        self.subir_todos(self.curso_seleccionado())

    # falla si no es un curso válido, es útil
    # para un try-catch
    def curso_seleccionado(self):
        assert self.sem_i >= 0 and self.cur_i >= 0
        return self.malla[self.sem_i]["cursos"][self.cur_i]

    def cambiar_seleccion(self, key):
        cur_bak = self.cur_i
        sem_bak = self.sem_i
        curso_bak = self.curso_seleccionado()
        if key == glfw.KEY_W:
            self.sem_i -= 1
        elif key == glfw.KEY_A:
            self.cur_i -= 1
        elif key == glfw.KEY_S:
            self.sem_i += 1
        elif key == glfw.KEY_D:
            self.cur_i += 1
        try:
            curso = self.curso_seleccionado()
        except:  # rollback changes
            self.sem_i = sem_bak
            self.cur_i = cur_bak
            return
        self.bajar_todos(curso_bak)
        self.subir_todos(curso)

    def subir_todos(self, curso):
        for i in curso["requisitos"]:
            self.view.update(i, REQUISITO)
        cod = curso["codigo"]
        self.view.update(cod, SELECCIONAR)
        for sem in self.malla:
            for cur in sem["cursos"]:
                if cod in cur["requisitos"]:
                    self.view.update(cur["codigo"], REQUISITO)

    def bajar_todos(self, curso):
        for i in curso["requisitos"]:
            self.view.update(i, BAJAR)
        cod = curso["codigo"]
        self.view.update(cod, BAJAR)
        for sem in self.malla:
            for cur in sem["cursos"]:
                if cod in cur["requisitos"]:
                    self.view.update(cur["codigo"], BAJAR)

    def over_selected(self):
        return np.array([0.5+self.cur_i, 0.5-self.sem_i, camera_distance])


class Controller:
    def __init__(self, json_filename):
        with open(json_filename) as json_file:
            self.malla = json.load(json_file)
        self.view = None
        self.model = None
        self.cam_pos = np.array([1, -2, camera_distance], dtype=np.float64)
        self.manual_movement = True
        self.trajectory = None
        self.progress = 0

    def init_view(self):
        self.view = View(self.malla)
        self.model = Model(self.malla, self.view)

    def on_key(self, window, key, scancode, action, mods):

        if action != glfw.PRESS:
            return

        if key == glfw.KEY_ESCAPE:
            sys.exit()
        elif key == glfw.KEY_1:
            self.manual_movement = True
            self.goto_selected()
        elif key == glfw.KEY_2:
            self.manual_movement = not self.manual_movement
            if not self.manual_movement:
                self.goto_selected()
        elif key in [glfw.KEY_W, glfw.KEY_A,glfw.KEY_S,glfw.KEY_D]:
            self.model.cambiar_seleccion(key)
            if not self.manual_movement:
                self.goto_selected()

    def goto_selected(self):
        P1 = np.array([self.cam_pos]).T
        P2 = np.array([self.model.over_selected()]).T
        T1 = np.array([[0, 1, 0]]).T
        T2 = np.array([[0, -1, 0]]).T
        self.trajectory = hermiteMatrix(P1, P2, T1, T2)
        self.progress = 0

    def show(self, pipeline):
        self.update_cam_pos()
        view = tr.lookAt(
            self.cam_pos,
            self.cam_pos + np.array([0, 0, -1]),
            np.array([0, 1, 0])
        )
        glUniformMatrix4fv(glGetUniformLocation(
            pipeline.shaderProgram, "view"), 1, GL_TRUE, view)
        self.view.show(pipeline)

    def update_cam_pos(self):
        t1 = glfw.get_time()
        try:
            dt = t1 - self.t0
        except AttributeError:
            pass
        self.t0 = t1

        if not self.manual_movement:
            self.follow_hermite(dt)
            return
        if (glfw.get_key(window, glfw.KEY_RIGHT) == glfw.PRESS):
            self.cam_pos[0] += 2 * dt
            self.trajectory = None
        if (glfw.get_key(window, glfw.KEY_LEFT) == glfw.PRESS):
            self.cam_pos[0] -= 2 * dt
            self.trajectory = None
        if (glfw.get_key(window, glfw.KEY_UP) == glfw.PRESS):
            self.cam_pos[1] += 2 * dt
            self.trajectory = None
        if (glfw.get_key(window, glfw.KEY_DOWN) == glfw.PRESS):
            self.cam_pos[1] -= 2 * dt
            self.trajectory = None

        if self.trajectory is not None:
            self.follow_hermite(dt)

    def follow_hermite(self, dt):
        if self.progress + dt < 1:
            self.progress += dt
            self.cam_pos = np.matmul(
                self.trajectory, generateT(self.progress))[:, 0]


def hermiteMatrix(P1, P2, T1, T2):
    # Generate a matrix concatenating the columns
    G = np.concatenate((P1, P2, T1, T2), axis=1)
    # Hermite base matrix is a constant
    Mh = np.array([[1, 0, -3, 2], [0, 0, 3, -2], [0, 1, -2, 1], [0, 0, -1, 1]])
    return np.matmul(G, Mh)


def generateT(t):
    return np.array([[1, t, t**2, t**3]]).T


def setup_glfw(controller):
    if not glfw.init():
        sys.exit()

    width = 800
    height = 600
    title = "Visor de la Malla DCC"

    window = glfw.create_window(width, height, title, None, None)

    if not window:
        glfw.terminate()
        sys.exit()

    glfw.make_context_current(window)
    glfw.set_key_callback(window, controller.on_key)
    return window


def setup_opengl():
    mvc_pipeline = es.SimpleTextureModelViewProjectionShaderProgram()
    # Telling OpenGL to use our shader program
    glUseProgram(mvc_pipeline.shaderProgram)
    # Setting up the clear screen color
    glClearColor(0.85, 0.85, 0.85, 1.0)
    # As we work in 3D, we need to check which part is in front,
    # and which one is at the back
    glEnable(GL_DEPTH_TEST)
    return mvc_pipeline


if __name__ == "__main__":
    texture_gen.generate("malla.json")
    controller = Controller("malla.json")
    window = setup_glfw(controller)
    pipeline = setup_opengl()
    controller.init_view()

    projection = tr.perspective(45, 800/600, 0.1, 100)
    glUniformMatrix4fv(glGetUniformLocation(
        pipeline.shaderProgram, "projection"), 1, GL_TRUE, projection)

    while not glfw.window_should_close(window):
        glfw.poll_events()
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        controller.show(pipeline)
        glfw.swap_buffers(window)
